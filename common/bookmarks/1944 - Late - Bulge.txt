bookmarks = {
	bookmark = {
		name = "BATTLE_OF_BULGE"
		desc = "GATHERING_STORM_DESC"
		date = 1944.12.28.12
		picture = "GFX_select_date_1945"
		default_country = "GER"
		default = no
		
		"GER"={
			history = "FRA_GATHERING_STORM_DESC"
			ideology = fascism
			ideas = {
			}
			focuses = {
				
			}
		}
		"USA"={
			history = "FRA_GATHERING_STORM_DESC"
			ideology = democratic
			ideas = {
			}
			focuses = {
				
			}
		}

		effect = {
			randomize_weather = 22345 # <- Obligatory in every bookmark !
			#123 = { rain_light = yes }
		}
	}
}
