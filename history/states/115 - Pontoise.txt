state={
	id=115
	name="STATE_115"
	provinces={
		1804 1932 2749 2827 3958 4891 5928 7232 7841 10362 10373 10391 10392 10416 10417 10424 10449 10455 10469 10482 10492 10504 10517 10526 10550 10557 
	}
	history = {
		buildings = {
			infrastructure = 10
			arms_factory = 2
			industrial_complex = 3
		}
		victory_points = { 7841 10 }
		 owner = GER
		 add_core_of = FRA

		1944.12.26 = {
			owner = USA
		}
	}
	manpower=1000000
	state_category = large_town
	buildings_max_level_factor=1.000
}
