state={
	id=162
	name="STATE_162"
	provinces={
		1275 1938 2248 4317 4468 5292 6149 6245 6676 6745 6937 6958 6973 7381 7462 8520 10987 11011 11031 11056 11080 11081 11099 11119 11142 11215 11248 11249 11286 11298 11326 11362 11391 11418 11434 11502 
	}
	manpower=1000000
	state_category = large_town
	buildings_max_level_factor=1.000

	history = {
		buildings = {
			infrastructure = 10
			arms_factory = 2
			industrial_complex = 3
			11080 = {
				naval_base = 4
			}
		}
		 = MRL
		 = MRL
	}
}
