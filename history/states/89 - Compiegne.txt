state={
	id=89
	name="STATE_89"
	provinces={
		143 1230 2019 2469 2918 3668 4272 5380 5741 6408 6453 7411 7994 9776 9780 9800 9816 9841 9859 9867 9897 9900 9907 9916 9953 9982 9988 9989 9994 10019 10020 10023 10038 10061 10062 10067 10071 10085 10105 10144 
	}
	history = {
		buildings = {
			infrastructure = 10
			arms_factory = 2
			industrial_complex = 3
		}
		 owner = GER
		 add_core_of = FRA
		1944.12.26 = {
			owner = USA
		}
	}
	manpower=1000000
	state_category = large_town
	buildings_max_level_factor=1.000
}
