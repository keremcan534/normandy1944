
state={
	id=101
	name="STATE_101"

	history={
		buildings = {
			infrastructure = 10
			arms_factory = 2
			industrial_complex = 3
			8001 = {
				naval_base = 4

			}

		}
		owner = GER
		add_core_of = FRA

		1944.12.26 = {
			owner = USA
		}
	}

	provinces={
		175 1262 1688 1822 1872 2117 2158 3095 3359 3941 4713 4719 5385 5537 6343 6358 6499 7141 7475 7740 7751 8001 8385 8448 8709 8743 8861 10130 10167 10194 10228 10251 10252 10258 10271 10290 10310 10337 10346 10353 10372 10390 10432 10443 10480 10508 10514 10532 10556 10574 10615 10643 
	}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category=large_town
}
