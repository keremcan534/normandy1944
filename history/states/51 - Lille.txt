state={
	id=51
	name="STATE_51"
	provinces={
		2045 2857 3440 3979 4004 4162 5702 6390 7397 7442 8691 9208 9214 9227 9234 9242 9249 9251 9259 9268 9269 9279 9288 9292 9301 9327 
	}
	history = {
		buildings = {
			infrastructure = 10
			arms_factory = 2
			industrial_complex = 3
		}
		victory_points = { 9249 10 }
		owner = GER
		 add_core_of = FRA

		1944.12.26 = {
			owner = USA
		}
	}
	manpower=1000000
	state_category = large_town
	buildings_max_level_factor=1.000
}
