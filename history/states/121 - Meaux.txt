state={
	id=121
	name="STATE_121"
	provinces={
		528 673 1730 2856 4374 5751 5841 6429 6596 6834 7005 10470 10498 10505 10518 10537 10538 10544 10551 10559 10576 10582 10591 10599 10600 10625 10636 10655 10659 10678 10713 10727 10798 10841 10884 
	}
	history = {
		buildings = {
			infrastructure = 10
			arms_factory = 2
			industrial_complex = 3
		}
		 owner = GER
		 add_core_of = FRA
		1944.12.26 = {
			owner = USA
		}
	}
	manpower=1000000
	state_category = large_town
	buildings_max_level_factor=1.000
}
