﻿capital = 206

oob = "standard_templates"

# Starting tech
set_technology = {
	infantry_weapons = 1
}

set_convoys = 0

set_politics = {	
	ruling_party = neutrality
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = yes
}

set_popularities = {
    democratic = 25
    fascism = 25
    communism = 25
    neutrality = 25
}
