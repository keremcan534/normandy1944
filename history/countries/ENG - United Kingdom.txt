﻿capital = 26

oob = "red"

# Starting tech
set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	support_weapons = 1
	tech_recon = 1
	tech_support = 1
	tech_engineers = 1
	tech_signal_company = 1
	tech_logistics_company = 1 
	paratroopers = 1
	tech_mountaineers = 1
	motorised_infantry = 1
	gw_artillery = 1
	interwar_antiair = 1
	interwar_antitank = 1
	gwtank = 1
	basic_light_tank = 1
	improved_light_tank = 1
	basic_heavy_tank = 1
        improved_medium_tank = 1
	early_fighter = 1
	fighter1 = 1
	early_bomber = 1
	tactical_bomber1 = 1
	strategic_bomber1 = 1
	CAS1 = 1
	naval_bomber1 = 1
	mobile_warfare = 1
	trade_interdiction = 1
	convoy_interdiction_ti = 1
	unrestricted_submarine_warfare = 1
	raider_patrols = 1
	synth_oil_experiments = 1
	support_weapons = 1
	tech_field_hospital = 1
	tech_military_police = 1
	electronic_mechanical_engineering = 1
	radio = 1
	fuel_silos = 1
	fuel_refining = 1
	jaegers = 1 
	shocktroops = 1 
}

add_ideas = full_export

set_convoys = 1000

set_politics = {	
	ruling_party = neutrality
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = yes
}

set_popularities = {
    democratic = 25
    fascism = 25
    communism = 25
    neutrality = 25
}
